var fs = require('fs');
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

//Save resource to resource.json
function getResource() {
    var request = new XMLHttpRequest();
    const URL = 'https://shoelace-dev-test.azurewebsites.net/api/UserProducts';
    request.open('GET', URL, true);

    request.onload = function() {
        try {
            var data = JSON.parse(this.responseText);
            fs.writeFile('resource.json', JSON.stringify(data), 'utf8');
        } catch (e) {
            console.error("Could not get resource");
        }
    }

    request.send();
}


/*==========================================
Template functions/classes
==========================================*/
var objectives = ["LeadGeneration", "Conversions", "Impressions"];

//Parent object
class Template {
    constructor(type, title, copy, objective) {
        this.type = type;
        this.title = title;
        this.copy = copy;
        this.objective = objective;
    }

    addCustomValue(key, value) {
        this[key] = value;
    }
}

class SIA extends Template {
    constructor(title="Default Title", copy="Default text", objective=objectives[0]) {
        super("Simple Image Ad", title, copy, objective);
    }
}

class MICA extends Template {
    constructor(title=["Default Title1", "Default Title2", "Default Title3"], copy=["Default Text1", "Default Text2", "Default Text3"], objective=objectives[1]) {
        super("Multi Image Carousel Ad", title, copy, objective);
    }

    //Generate array of values from specified field (this could probably be in a MultiImageTemplate parent object)
    addToCampaignFromResource(start, end, field) {
        var resource = JSON.parse(fs.readFileSync('resource.json')).splice(start,end);
        var values = resource.map(function(value) {
            return value[field];
        })
        this.addCustomValue(field == "Name" ? "title" : "copy", values);
    }
}

class MISA extends Template {
    constructor(title=["Default Title1", "Default Title2", "Default Title3"], copy=["Default Text1", "Default Text2", "Default Text3"], objective=objectives[2]) {
        super("Multi Image Slider Ad", title, copy, objective);
    }
}

//Generate campaign from template
function generateCampaign(type) {
    if(type == "SIA") {
        return new SIA();
    } else if (type == "MICA") {
        return new MICA();
    } else if (type == "MISA") {
        return new MISA();
    }
}

//Dummy save function
function save(campaigns) {
    console.log("Saving campaigns...");
    campaigns.forEach(element => {
        console.log(element);
    });
}

//Dummy publish function
function publish() {
    console.log("Publishing...");
    setTimeout(function() {
        console.log("Done!")
    }, 3000);
}

//Create campaign and add custom values specified in params
function createCampaign(templateType, params) {
    var campaign = generateCampaign(templateType);
    for(var key in params) {
        campaign.addCustomValue(key, params[key]);
    }

    return campaign;
}

/*==========================================
Main
==========================================*/
function main() {
    if(!fs.existsSync('resource.json')) {
        console.log("Getting resource...")
        getResource(); //Not able to handle async yet ... :/
    }

    //Custom values for campaign1
    const custom1 = {
        "Image": "https://shoelaceproductimages.s3.amazonaws.com/c67bda95-4fc6-498c-9022-9f9105e35f51.jpg",
        "Status": "Paused",
        "Ad Network ID": ""
    }

    //Custom values for campaign2
    const custom2 = {
        "Images": ["https://shoelaceproductimages.s3.amazonaws.com/2f4ea7f4-f63a-4c78-a59b-5266fb20d873.jpg",
        "https://shoelaceproductimages.s3.amazonaws.com/7db18e9d-88f8-4f87-9cee-2705e1e8140f.jpg",
        "https://shoelaceproductimages.s3.amazonaws.com/4cd68533-fadc-4973-83b6-4a577f603f8e.jpeg"],
        "Status": "Implemented",
        "Ad Network ID": ""
    }

    var campaign1 = createCampaign("SIA", custom1);
    var campaign2 = createCampaign("MICA", custom2);
    campaign2.addToCampaignFromResource(0,3, "Name");
    campaign2.addToCampaignFromResource(0,3, "Description");
    save([campaign1, campaign2]);
    publish();
}

main();